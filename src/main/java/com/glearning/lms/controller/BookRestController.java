package com.glearning.lms.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.glearning.lms.model.Book;
import com.glearning.lms.service.BookService;

@RestController
@RequestMapping("/api/books")
public class BookRestController {
	
	@Autowired
	private BookService bookService;
	
	
	@GetMapping
	public Set<Book> fetchBooks(){
		return this.bookService.findAll();
	}
	
	@GetMapping("/{id}")
	public Book findBookById(@PathVariable("id") int id) {
		return this.bookService.findById(id);
	}
	
	@PostMapping
	public Book saveBook(@RequestBody Book book) {
		return this.bookService.save(book);
	}
	
	@DeleteMapping("/{id}")
	public void deleteBook(@PathVariable("id") int id) {
		this.bookService.deleteById(id);
	}
	

}
