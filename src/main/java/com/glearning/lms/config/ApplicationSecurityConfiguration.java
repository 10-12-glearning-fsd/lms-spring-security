package com.glearning.lms.config;

import org.springframework.context.annotation.Configuration;

@Configuration
//old way
public class ApplicationSecurityConfiguration  {//extends WebSecurityConfigurerAdapter {
/*
	@Autowired
	private UserDetailsService userDetailsService;

	// for authentication
	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(this.userDetailsService).passwordEncoder(passwordEncoder());

	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.cors().disable();
		http.csrf().disable();
		http.headers().frameOptions().disable();
		http.authorizeRequests()
				.antMatchers(HttpMethod.GET, "/api/books**")
					.hasAnyRole("USER", "ADMIN")
				.antMatchers(HttpMethod.POST, "/api/books**")
					.hasAnyRole("ADMIN")
				.antMatchers(HttpMethod.DELETE, "/api/books/**")
					.hasAnyRole("ADMIN")
				.anyRequest()
					.fullyAuthenticated()
				.and()
				.httpBasic();
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
*/
}
