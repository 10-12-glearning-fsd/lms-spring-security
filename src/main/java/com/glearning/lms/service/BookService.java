package com.glearning.lms.service;

import java.util.Set;

import com.glearning.lms.model.Book;

public interface BookService {
	
	public Set<Book> findAll();

	public Book findById(int theId);

	public Book save(Book theBook);

	public void deleteById(int theId);

	public Set<Book> searchBy(String name, String author);


}
