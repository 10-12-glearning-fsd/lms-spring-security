package com.glearning.lms.service;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.glearning.lms.dao.BookJpaRepository;
import com.glearning.lms.model.Book;

@Service
public class BookServiceImpl implements BookService {
	
	@Autowired
	private BookJpaRepository bookRepository;

	@Override
	public Set<Book> findAll() {
		return Set.copyOf(this.bookRepository.findAll());
	}

	@Override
	public Book findById(int bookId) {
		return this.bookRepository.findById(bookId).orElseThrow(() -> new IllegalArgumentException("invalid book id passed"));
	}

	@Override
	public Book save(Book book) {
		return this.bookRepository.save(book);
	}

	@Override
	public void deleteById(int bookId) {
		this.bookRepository.deleteById(bookId);
		
	}

	@Override
	public Set<Book> searchBy(String name, String author) {
		return this.bookRepository.findByNameContainsAndAuthorContainsAllIgnoreCase(name, author);
	}

}
