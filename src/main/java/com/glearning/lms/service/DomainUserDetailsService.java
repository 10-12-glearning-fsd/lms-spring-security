package com.glearning.lms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.glearning.lms.dao.UserJpaRepository;
import com.glearning.lms.model.DomainUserDetails;

@Service
@Primary
public class DomainUserDetailsService implements UserDetailsService {
	
	@Autowired
	private UserJpaRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return this.userRepository
							.findByName(username)
							.map(DomainUserDetails:: new)
							.orElseThrow(() -> new UsernameNotFoundException("bad credentials"));
	}

}


