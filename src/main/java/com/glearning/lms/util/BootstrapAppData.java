package com.glearning.lms.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import com.glearning.lms.dao.BookJpaRepository;
import com.glearning.lms.dao.UserJpaRepository;
import com.glearning.lms.model.Book;
import com.glearning.lms.model.Role;
import com.glearning.lms.model.User;

@Configuration
public class BootstrapAppData {
	@Autowired
	private BookJpaRepository bookRepository;
	
	@Autowired
	private UserJpaRepository userRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	public BootstrapAppData(BookJpaRepository bookRepository, UserJpaRepository userRepository, PasswordEncoder passwordEncoder) {
		this.bookRepository = bookRepository;
		this.userRepository = userRepository;
		this.passwordEncoder = passwordEncoder;
	}
	
	@EventListener(ApplicationReadyEvent.class)
	@Transactional
	public void onReady(ApplicationReadyEvent event) {
		Book book1 = Book.builder().author("Vinay").name("Small is Big").category("Motivation").build();
		Book book2 = Book.builder().author("Harish").name("10X").category("Motivation").build();
		
		this.bookRepository.save(book1);
		this.bookRepository.save(book2);
		
		// addding users and roles
		
		User kiran = new User("kiran", this.passwordEncoder.encode("welcome"));
		User vinay = new User("vinay", this.passwordEncoder.encode("welcome"));
		
		Role userRole = new Role("ROLE_USER");
		Role adminRole = new Role("ROLE_ADMIN");
		
		kiran.addRole(userRole);
		
		vinay.addRole(userRole);
		vinay.addRole(adminRole);
		
		this.userRepository.save(kiran);
		this.userRepository.save(vinay);

	}

}
